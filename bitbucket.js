const getJson = require('./httpget')
var request = require('request')

const options = {
    host: 'api.bitbucket.org',
    port: 443,
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
}

const pathBase = '/2.0/repositories/fauna5/swaggertest/'

exports.getTags = function (callback) {
    console.log('getting tags...')
    const httpConfig = Object.assign({ path: pathBase + 'refs/tags' }, options)
    getJson.getJSON(httpConfig, (err, statusCode, data) => {
        if (err) {
            console.log('error getting tags', err)
            throw err
        } else {
            const tags = data.values.map((tag) => {
                return { "name": tag.name, "hash": tag.target.hash, "date": tag.target.date }
            })
            console.log('got tags: ', tags.map((tag) => {return tag.name}).join(", "))
            callback(tags)
        }
    })
}

exports.getBranches = function (callback) {
    console.log('getting branches...')
    const httpConfig = Object.assign({ path: pathBase + 'refs/branches' }, options)
    getJson.getJSON(httpConfig, (err, statusCode, data) => {
        if (err) {
            console.log('error getting branches', err)
            throw err
        } else {
            const branches = data.values.map((branch) => {
                return { "name": branch.name, "hash": branch.target.hash, "date": branch.target.date }
            })
            console.log('got branches: ', branches.map((branch) => {return branch.name}).join(", "))
            callback(branches)
        }
    })
}

exports.createBranch = function (branchName) {
    console.log('creating branch...')
    const formData = {
        repository: 'fauna5/swaggertest',
        from_branch: 'master',
        branch_name: branchName
    }
    const auth = {
        'user': 'fauna5',
        'pass': 'C0ngress!'
    }
    request.post({ url: 'https://bitbucket.org/branch/create', formData: formData, auth:auth }, (err, httpResponse, body) => {
        if (err) {
            return console.error('Branch failed:', err);
        }
        if (httpResponse.statusCode === 302) {
            console.log('Branch successful: ', httpResponse.statusCode);    
        } else {
            console.log('Branch failed server responded with: ', httpResponse.statusCode);
        }
    });
}