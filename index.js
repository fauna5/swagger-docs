const express = require('express')
const bitbucket = require('./bitbucket')
var bodyParser = require('body-parser');

const app = express()
app.use(express.static('public'))
app.use('/swagger-ui', express.static('node_modules/swagger-ui/dist'))

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/versions', function (req, res) {
    bitbucket.getTags((tags) => {
        bitbucket.getBranches((branches) => {
            res.send(JSON.stringify({tags, branches}))
            res.end()
        })
    })
})

app.post('/branch', function (req, res) {
    console.log('branch', req.body.branch_name)
    bitbucket.createBranch(req.body.branch_name)
})

app.listen(3000, function () {
    console.log('Server running on port 3000')
})
bitbucket.createBranch('bob2');